#!/bin/bash

# default arg init
lowUser=$1
basedirpath=$(cd "$(dirname "$0")"; pwd)


if [ $# -lt 1 ]; then
    echo $0" [username]"
    exit
fi

## check || restart disguiser_agent
echo $basedirpath/disguiser_agent
flock -xn /tmp/disguiser_agent_run_lock $basedirpath/disguiser_agent $basedirpath/patch/config.json > /dev/null 2>&1 &


## iptables rules init
function SetIptables () {
    check=$(echo $@ | sed -e 's/-A/-C/g')
    #echo $check
    iptables $check ; ret=$?
    if [ "$ret" -ne 0 ]; then
        echo "NO";
        iptables $@
    fi
}

SetIptables -A OUTPUT -p tcp --tcp-flags RST RST -j DROP
SetIptables -A OUTPUT -p icmp --icmp-type port-unreachable -j DROP

######  root's log -> ubuntu's log
######
# RightReduction [username] [srcdir] [dstdir]
function RightReduction() {
    lowUser=$1
    FromDir=$2
    ToDir=$3
    
    test -d $ToDir || mkdir -p $ToDir
    test -d $ToDir && chmod 0777 $ToDir
    test -f $ToDir && chown $lowUser:$lowUser $ToDir
    for file in `ls $FromDir`
    do
        frompath=$FromDir/$file
        topath=$ToDir/$file
        test -f $frompath && chmod 0777 $frompath
        test -f $frompath && chown $lowUser:$lowUser $frompath
        test -f $frompath && mv $frompath $topath
    done
}

RightReduction $lowUser /tmp/disguiser_with_root /tmp/LogTest

######  ubuntu's patch-file -> root's patch-file -> disguiser_agent
######
# PatchFileSyn [fromDir] [toDir] [filename]
function PatchFileSyn() {
    fromFile=$1/$3
    toFile=$2/$3

    test -d $2 || mkdir -p $2
    test -d $2 && chmod 0777 $2
    test -f $fromFile && chmod 0777 $fromFile && mv $fromFile $toFile
    test -f $toFile   && chown root:root $toFile
}

PatchFileSyn /tmp/patchFiles $basedirpath/patch portscan.json
PatchFileSyn /tmp/patchFiles $basedirpath/patch patch_fwd.json
