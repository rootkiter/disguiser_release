#!/bin/python
###############################################
# File Name : install.py
#    Author : rootkiter
#    E-mail : rootkiter@rootkiter.com
#   Created : 2017-10-30 05:38:28 PDT
###############################################

import json,httplib
import sys,os,socket

def copyFile ( infilepath, outdir, overwrite=True ):
    # check outdir
    dirtest="test -d %s || mkdir -p %s" % (outdir, outdir)
    print(dirtest)
    os.system(dirtest)
    if overwrite:
        cpcmd = "test -f %s && cp %s %s" % (
            infilepath, infilepath, outdir
        )
        print (cpcmd)
        os.system(cpcmd)
    else:
        filename = os.path.basename(infilepath)
        outfilepath= os.path.join(outdir, filename)
        cpcmd = "test -f %s || (test -f %s && cp %s %s)" % (
            outfilepath,
            infilepath,
            infilepath,
            outfilepath
        )
        print(cpcmd)
        os.system(cpcmd)

def moveFile ( srcfile, dstDir, overwrite = True ):
    dirtest="test -d %s || mkdir -p %s" % (dstDir, dstDir)
    print(dirtest)
    os.system(dirtest)

    if overwrite:
        mvcmd = "test -f %s && test -d %s && mv %s %s" % (
            srcfile, dstDir, srcfile, dstDir
        )
        print (mvcmd)
        os.system(mvcmd)


def get_http(host,url):
    conn = httplib.HTTPConnection(host)
    conn.request(method="GET",url=url) 
    response = conn.getresponse()
    res= response.read()
    return res.strip()


def getLip(ifacename):
    import fcntl,struct
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    hostip  = socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifacename[:15])
    )[20:24])
    return hostip

def get_gateMacIpv4():
    routcmd = "cat /proc/net/route"
    routresult = os.popen(routcmd).read().split('\n')
    interface = []
    hexip    = []
    buf = {}
    for i in routresult:
        items = i.split()
        if(len(items) != 11):
            continue
        if(items[1] == '00000000'):
            if(items[0] not in interface):
                interface.append(items[0])
            if(items[2] not in hexip):
                hexip.append(items[2])
    if(len(interface) != 1 or len(hexip) != 1):
        buf['interface'] = str(interface)
        buf['hexip']     = str(hexip)
        return False,buf

    IPaddr = ".".join((lambda x:[ str(ord((x[len(x)-2*(1+i)]+x[len(x)-2*(i+1)+1]).decode('hex'))) for i in range(0,len(x)/2)])(hexip[0]))

    arpcmd = "cat /proc/net/arp"
    macaddr = []
    for item in os.popen(arpcmd).read().split("\n"):
        dicobj = item.split()
        if(len(dicobj) > 0):
            if( dicobj[0] == IPaddr):
                if(dicobj[3] not in macaddr):
                    macaddr.append(dicobj[3])

    if(len(macaddr) != 1):
        buf['interface'] = str(interface)
        buf['hexip']     = str(hexip)
        buf['macaddr']   = str(macaddr)
        return False,buf

    buf['facename'] = interface[0]
    buf['gateway' ] = IPaddr
    buf['macaddr' ] = macaddr[0]

    buf['hostip'  ] = getLip(interface[0])
    return True,buf

def configInit( outpath , releasepath):
    config = {}
    config["pubip"] = get_http("ipecho.net","http://ipecho.net/plain")

    #config["blackip"]= ["47.89.243.200"]
    #config["blackport"]= ["2790"]

    macflag,buf = get_gateMacIpv4()
    if(macflag):
        config['facename'] = buf['facename']
        #config['gateip'  ] = buf['gateway' ]
        #config['gatemac' ] = buf['macaddr' ]
        config['hostip'  ] = buf['hostip'  ]

    config["scan"]      = True
    config["control"]   = "35.184.91.65"
    config["blockPort"] = [2790, 21337]
    config["v2"]        = "newDis"
    config["portscan"]  = os.path.join(releasepath, "patch/portscan.json")
    config["fwdPool"]   = os.path.join(releasepath, "portpool/")
    config["fwdPatch"]  = os.path.join(releasepath, "patch/patch_fwd.json")


    outdir = outpath
    outdir = os.path.join(outdir,'config')
    if(not os.path.exists(outdir)):
        os.makedirs(outdir)

    with open(os.path.join(outdir,'config.json'),'w') as f:
        f.write(json.dumps(config)+"\n")

    print config

def updateSh( releasepath , outpath ):
    module = "DIS_RELEASE_PATH=%s\n" % releasepath
    module+= "test -d $DIS_RELEASE_PATH || (echo \"NO DIR\" && exit)\n"
    module+= "cd $DIS_RELEASE_PATH && git config --global user.email \"rootkiter@rootkiter.com\" && git stash && git pull && python ./install.py\n"


    with open(os.path.join(outpath,'update.sh'),'w') as f:
        f.write(module)


if __name__=='__main__1':
    #print(get_gateMacIpv4())
    print get_http("ipecho.net","http://ipecho.net/plain")
    #updateSh("HELLO")

if __name__=='__main__':
    update  = False
    if("clean" in sys.argv):
        pathlist = [
            "/tmp/*dat2",
            "/tmp/*dat3",
            "/tmp/*dat4",
            #"/tmp/LogTest",
            "/tmp/disguiser_*",
            "/tmp/portlog_*",
        ]
        for cleanpath in pathlist:
            cmd = "rm -rf "+ cleanpath
            os.system(cmd)
        exit(0)

    release_dir = os.path.dirname(os.path.abspath(__file__))


    #os.system("/root/go/bin/go build src/main/disguiser.go")
    homepath = os.popen("cd ~ && pwd").read().strip()

    outdir   = os.path.join(homepath,"disguiser_go")
    patchdir = os.path.join(outdir  ,"patch"      )

    configInit( release_dir, outdir )
    copyFile( "config/config.json"  , patchdir, False )  # Don't overwrite
    copyFile( "config/portscan.json", patchdir, False )  # Don't overwrite
    copyFile( "./start.sh"          , outdir          )  # update start.sh everytime

    moveFile( "./disguiser_agent"   , outdir          )  # move && overwrite

    updateSh (release_dir, outdir )    # update bash init

    if(not os.path.exists(os.path.join(outdir,"portpool"))):
        os.system("cd "+outdir+" && git clone https://rootkiter@bitbucket.org/rootkiter/new_portpool_for_disguiser.git newpool")
    elif(update):
        os.system("cd "+outdir+" && cd newpool && git pull")

    print ("$ echo \"*/5 *   * * *   root    "+outdir+"/start.sh\" > /etc/crontab")
    print ("$ echo \"*/5 *   * * *   "+outdir+"/start.sh [username]\" > crontab -e")
    print ("$ ps -ef | grep dis | grep agent | awk '{system(\"kill -9 \"$2)}' && ~/disguiser_go/start.sh [username]")
